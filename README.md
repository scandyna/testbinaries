# TestBinaries

Provides some binaries usable for some tests.

It is meant to be used with
[CMake ExternalData](https://cmake.org/cmake/help/latest/module/ExternalData.html).

The package registry can be browsed
[here](https://gitlab.com/scandyna/testbinaries/-/packages/).

Notice that the URL given while browsing the registry is not meaningful.

To fetch the contents, the URL format is:
```
https://gitlab.com/api/v4/projects/40217884/packages/generic/$PACKAGE_NAME/$VERSION/$FILE
```

## Usage with CMake ExternalData

If you are new to CMake ExternalData, you should take a look at the links referenced below.

In your source tree, create a directory named `TestBinaries`.
In this directory, put the text files that are of interest.

Simple project layout example:
```
project
  |-src
  |-tests
      |-src
      |-TestBinaries
      |     |-lib_linux_ubuntu_18.04_gcc_8_ld_bfd_x86_64_no_rpath_Debug.so.md5
      |     |-linux_ubuntu_18.04_gcc_8_ld_bfd_x86_64_no_rpath_Debug.md5
      |-CMakeLists.txt
```
In the `TestBinaries`, each file is a text file
that contains the MD5 hash of the original binary.

Note: here we have to download the files manually.
This can be automated.
See the [SimpleProject example](examples/SimpleProject/)
for more informations.

In the `CMakeLists.txt`:
```cmake
set(TEST_BINARIES_PACKAGE_BASE_URL "https://gitlab.com/api/v4/projects/40217884/packages/generic")
set(TEST_BINARIES_PACKAGE_VERSION "0.0.1")

set(ExternalData_URL_TEMPLATES
    "${TEST_BINARIES_PACKAGE_BASE_URL}/%(algo)/${TEST_BINARIES_PACKAGE_VERSION}/%(hash)"
)

ExternalData_Expand_Arguments(TestBinaries
  TEST_BINARIES_PATH_LIST
  DATA{binaries/lib_linux_ubuntu_18.04_gcc_8_ld_bfd_x86_64_no_rpath_Debug.so}
  DATA{binaries/linux_ubuntu_18.04_gcc_8_ld_bfd_x86_64_no_rpath_Debug}
)
ExternalData_Add_Target(TestBinaries)
```

# Useful documentation

## Using CMake ExternalData

- https://crascit.com/2015/04/03/handling-binary-assets-and-test-data-with-cmake/
- https://cmake.org/cmake/help/book/mastering-cmake/chapter/Testing%20With%20CMake%20and%20CTest.html

## Some technical links

- https://stackoverflow.com/questions/1867745/cmake-use-a-custom-linker
