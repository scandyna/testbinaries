#include "sharedlibrary_export.h"
#include <iostream>

SHAREDLIBRARY_EXPORT
void sharedLibraryFunction()
{
  std::cout << "Hello from shared library" << std::endl;
}
