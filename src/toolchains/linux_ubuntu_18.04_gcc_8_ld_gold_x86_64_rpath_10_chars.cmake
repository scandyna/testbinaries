
set(CMAKE_CXX_COMPILER g++-8)
set(CMAKE_CXX_FLAGS_INIT -fuse-ld=gold)

set(CMAKE_BUILD_RPATH "XXXXXXXXXX")

set(SHARED_LIBRARY_NAME "_linux_ubuntu_18.04_gcc_8_ld_gold_x86_64_rpath_10_chars_$<CONFIG>")
set(EXECUTABLE_NAME "linux_ubuntu_18.04_gcc_8_ld_gold_x86_64_rpath_10_chars_$<CONFIG>")
