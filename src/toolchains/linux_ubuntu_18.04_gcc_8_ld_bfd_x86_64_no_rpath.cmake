
set(CMAKE_CXX_COMPILER g++-8)
set(CMAKE_CXX_FLAGS_INIT -fuse-ld=bfd)

set(CMAKE_SKIP_RPATH TRUE)

set(SHARED_LIBRARY_NAME "_linux_ubuntu_18.04_gcc_8_ld_bfd_x86_64_no_rpath_$<CONFIG>")
set(EXECUTABLE_NAME "linux_ubuntu_18.04_gcc_8_ld_bfd_x86_64_no_rpath_$<CONFIG>")
