#!/bin/bash

# GitLab docs: https://docs.gitlab.com/ee/user/packages/generic_packages
#
# The URL format:
# PUT /projects/:id/packages/generic/:package_name/:package_version/:file_name?status=:status

uploadFile()
{
  file="$1"
  packageName="$2"
  packageVersion="$3"
  destinationUrl="$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/$packageName/$packageVersion/$file"

  echo "uploading $file to $destinationUrl ..."

  curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$file" "$destinationUrl"

  exitCode=$?
  if [ $exitCode -ne 0 ]
  then
      echo "[ERROR] uploading $file failed with code $exitCode"
      exit $exitCode
  fi

}

cd MD5

for fileName in *
do
  if [ -f "$fileName" ]
  then
    uploadFile "$fileName" "MD5" "$VERSION_DIR"
  fi
done

cd ..
cd src

for fileName in *
do
  if [ -f "$fileName" ]
  then
    uploadFile "$fileName" "src" "$VERSION_DIR"
  fi
done

cd ..
cd matrix

uploadFile "matrix.csv" "matrix" "$VERSION_DIR"
