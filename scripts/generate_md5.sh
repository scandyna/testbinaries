#!/bin/bash

matrixFileOutpuDir="matrix"
matrixFile="$matrixFileOutpuDir/matrix.csv"

mkdir "MD5"
mkdir "src"
mkdir "$matrixFileOutpuDir"

echo "File,MD5" > $matrixFile

for fileName in *
do
  if [ -f "$fileName" ]
  then
    echo "processing $fileName ..."
    fileSum=$(md5sum $fileName | cut -d ' ' -f 1)
    mv "$fileName" "MD5/$fileSum"
    echo "$fileSum" > "src/$fileName.md5"
    echo "$fileName,$fileSum" >> $matrixFile
  fi
done
