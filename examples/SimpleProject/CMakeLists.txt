
cmake_minimum_required(VERSION 3.15)
project(SimpleProject LANGUAGES CXX)

enable_testing()

add_subdirectory(tests)
